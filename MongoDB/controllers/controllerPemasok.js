const { ObjectId } = require("mongodb");
const client = require("../models");

class ControllerPemasok {
  async getAll(req, res) {
    try {
      const dbConnect = client.db("penjualan_afternoon_MongoDB");
      const pemasok = dbConnect.collection("pemasok");
      let data = await pemasok.find({}).toArray();
      if (data.length === 0) {
        return res.status(404).json({
          message: "pemasok not found!",
        });
      }
      return res.status(200).json({
        message: "get pemasok success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "server error",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      const dbConnect = client.db("penjualan_afternoon_MongoDB");
      const pemasok = dbConnect.collection("pemasok");
      let data = await pemasok.findOne({ _id: new ObjectId(req.params.id) });
      return res.status(200).json({
        message: "get pemasok success!",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "server error",
        error: e,
      });
    }
  }

  async create(req, res) {
    try {
      const dbConnect = client.db("penjualan_afternoon_MongoDB");
      const pemasok = dbConnect.collection("pemasok");
      let data = await pemasok.insertOne({
        nama: req.body.nama,
      });
      return res.status(200).json({
        message: "post pemasok success",
        data: data.ops[0],
      });
    } catch (e) {
      return res.status(500).json({
        message: "server error",
        error: e,
      });
    }
  }

  async update(req, res) {
    try {
      const dbConnect = client.db("penjualan_afternoon_MongoDB");
      const pemasok = dbConnect.collection("pemasok");
      await pemasok.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
          },
        }
      );
      let data = await pemasok.findOne({ _id: new ObjectId(req.params.id) });
      return res.status(200).json({
        message: "put pemasok success!",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async delete(req, res) {
    try {
      const dbConnect = client.db("penjualan_afternoon_MongoDB");
      const pemasok = dbConnect.collection("pemasok");
      let data = await pemasok.deleteOne({
        _id: new ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "delete pemasok success",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new ControllerPemasok();
