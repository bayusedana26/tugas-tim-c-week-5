const { ObjectId } = require("mongodb");
const client = require("../models/index");
const penjualan = client.db("penjualan_timC"); // Connect to penjualan database
const transaksi = penjualan.collection("transaksi"); // Connect to transaksi collection / table

class TransaksiController {
  // get All data
  async getAll(req, res) {
    try {
      // find all transaksi data
      let data = await transaksi.find({}).toArray();

      //   if no data found
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi data not found",
        });
      }
      return res.status(200).json({
        message: "Succes",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // get one data
  async getOne(req, res) {
    try {
      // Find one data
      let data = await transaksi.findOne({
        id: new ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // Create data
  async create(req, res) {
    try {
      // Insert data transaksi
      let data = await transaksi.insertOne({
        barang: req.body.barang,
        pelanggan: req.body.pelanggan,
        jumlah: req.body.jumlah,
        total: req.body.total,
      });

      // If success
      return res.status(200).json({
        message: "Success",
        data: data.ops[0],
      });
    } catch (e) {
      console.log(e);
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Update data
  async update(req, res) {
    try {
      // Update data transaksi
      await transaksi.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            barang: req.body.barang,
            pelanggan: req.body.pelanggan,
            jumlah: req.body.jumlah,
            total: req.body.total,
          },
        }
      );

      // Find data that updated
      let data = await transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Delete Data
  async delete(req, res) {
    try {
      // delete data depends on req.params.id
      let data = await transaksi.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Success to delete transaksi",
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
