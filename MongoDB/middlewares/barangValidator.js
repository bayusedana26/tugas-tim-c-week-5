const validator = require("validator");
const { ObjectId } = require("mongodb");
const connection = require("../models");

exports.create = async (req, res, next) => {
  try {
    const dbConnection = connection.db("penjualan_afternoon"); // Connect to penjualan database
    const barang = dbConnection.collection("barang"); // Connect to transaksi collection / table
    // Find pemasok
    let findPemasok = await dbConnection.collection("pemasok").findOne({
      _id: new ObjectId(req.body.id_pemasok),
    });

    // Create errors variable
    let errors = [];

    // If barang not found
    if (!findPemasok) {
      errors.push("Pemasok Not Found");
    }

    // Check harga is number
    if (!validator.isNumeric(req.body.harga)) {
      errors.push("Harga must be a number");
    }

    // check nama barang is huruf
    if (!validator.isAlpha(req.body.nama, ["en-US"], { ignore: " " })) {
      errors.push("Nama barang must be huruf");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    const dbConnection = connection.db("penjualan_afternoon"); // Connect to penjualan database
    const barang = dbConnection.collection("barang"); // Connect to barang collection / table

    let findData = await Promise.all([
      barang.findOne({
        _id: new ObjectId(req.body.id_barang),
      }),
      dbConnection.collection("pemasok").findOne({
        _id: new ObjectId(req.body.id_pemasok),
      }),
      barang.findOne({
        _id: new ObjectId(req.params.id),
      }),
    ]);

    // Create errors variable
    let errors = [];

    // Check harga is number
    if (!validator.isNumeric(req.body.harga)) {
      errors.push("Harga must be a number");
    }

    // check nama barang is huruf
    if (!validator.isAlpha(req.body.nama, ["en-US"], { ignore: " " })) {
      errors.push("Nama barang must be huruf");
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};
