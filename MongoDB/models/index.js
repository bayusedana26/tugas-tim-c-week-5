const { MongoClient } = require("mongodb"); // Import mongodb

const uri = process.env.MONGO_URI; // uri of mongodb in our computer for connection
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
// Make new client /
try {
  client.connect(); // Make connection to mongodb
  console.log("Connected to MongoDB!");

} catch (e) {
  console.error(e);
}

module.exports = client; // Export client /
