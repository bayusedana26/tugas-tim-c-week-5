const express = require("express");
const routes = express.Router();
const controllerTransaksi = require("../controller/controllerTransaksi");

routes.get("/", controllerTransaksi.getALL);
routes.get("/:id", controllerTransaksi.getOne);

module.exports = routes;
