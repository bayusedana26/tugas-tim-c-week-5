const express = require("express"); // Import express


// Import validator
const barangValidator = require("../middlewares/barangValidator");
// Import controller
const barangController = require("../controllers/barangController");

//make router
const router = express.Router();

router.post("/", barangValidator.create, barangController.create);
router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.put("/:id", barangValidator.update, barangController.update);
router.delete("/:id", barangController.delete);

module.exports = router; // Export router
