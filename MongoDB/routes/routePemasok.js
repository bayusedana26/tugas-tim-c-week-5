const express = require("express");
const validator = require("../middlewares/validator/validatorPemasok");
const routes = express.Router();


const controllerPemasok = require("../controllers/controllerPemasok");

routes.get("/", controllerPemasok.getAll);
routes.get("/:id", controllerPemasok.getOne);
routes.post("/", validator.create, controllerPemasok.create);
routes.put("/:id", validator.create, controllerPemasok.update);
routes.delete("/:id", controllerPemasok.delete);
module.exports = routes;
