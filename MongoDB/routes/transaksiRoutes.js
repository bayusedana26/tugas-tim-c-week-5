const express = require("express"); // Import express
const router = express.Router(); // Make express router
const TransaksiController = require("../controllers/transaksiControllers"); // Import transaksiController from controllers directory
const transaksiValidator = require("../middlewares/transaksiValidator.js"); // Import transaksiValidator

router.get("/", TransaksiController.getAll);
router.get("/:id", TransaksiController.getOne);
router.post("/", transaksiValidator.create, TransaksiController.create);
router.put("/:id", transaksiValidator.update, TransaksiController.update); //
router.delete("/delete/:id", TransaksiController.delete);

module.exports = router; // Export router
