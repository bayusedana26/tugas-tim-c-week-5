const crypto = require("crypto");
const path = require("path");
const { barang, pelanggan, pemasok, transaksi } = require("../models");

class PemasokController {
  async getAll(req, res) {
    try {
      let data = await pemasok.find();
      if (data.length === 0) {
        return res.status(404).json({
          message: "data not found",
        });
      }

      return res.status(200).json({
        message: "sukses",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "server error not found",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      let data = await pemasok.findOne({ _id: req.params.id });

      if (!data) {
        return res.status(404).json({
          message: "Transaksi not found",
        });
      }

      return res.status(200).json({
        message: "sukses",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "server error not found",
        error: e,
      });
    }
  }

  async create(req, res) {
    // If image was uploaded
    try {
      if (req.files) {
        const file = req.files.image;

        // Make sure image is photo
        if (!file.mimetype.startsWith("image")) {
          return res.status(400).json({ message: "File must be an image" });
        }

        // Check file size (max 1MB)
        if (file.size > 1000000) {
          return res
            .status(400)
            .json({ message: "Image must be less than 1MB" });
        }

        // Create custom filename
        let fileName = crypto.randomBytes(16).toString("hex");

        // Rename the file
        file.name = `${fileName}${path.parse(file.name).ext}`;

        // assign req.body.image with file.name
        req.body.image = file.name;

        // Upload image to /public/images
        file.mv(`./public/images/${file.name}`, async (err) => {
          if (err) {
            console.error(err);

            return res.status(500).json({
              message: "image failed, server error",
              error: err,
            });
          }
        });
      }

      let createdData = await pemasok.create(req.body);

      let data = await pemasok.findOne({ _id: createdData._id });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      console.error(e);
      return res.status(500).json({
        message: "Crate failed, Internal Server Error",
        error: e,
      });
    }
  }

  async update(req, res) {
    try {
      let data = await pemasok.findOneAndUpdate(
        { _id: req.params.id },
        { nama: req.body.nama, image: req.files.image },
        { new: true }
      );

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      console.error(e);
      return res.status(500).json({
        message: "update failed, Internal Server Error",
        error: e,
      });
    }
  }

  async delete(req, res) {
    try {
      await pemasok.delete({ _id: req.params.id });
      return res.status(200).json({
        message: "delete success",
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "delete error, server not found",
        error: e,
      });
    }
  }
}

module.exports = new PemasokController();
