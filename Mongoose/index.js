require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
// Express
const express = require("express");
const fileUpload = require("express-fileupload");

// Make router
const transaksiRoutes = require("./routes/transaksiRoute");
const pemasokRoutes = require("./routes/pemasokRoute");
const pelangganRoutes = require("./routes/pelangganRoute");
const barangRoutes = require("./routes/barangRoute");

// Make application
const app = express();

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: true })); // enable req body urlencoded

// Read form data
app.use(fileUpload());

// static folder (for images)
app.use(express.static("public"));

// Use routes
app.use("/transaksi", transaksiRoutes);
app.use("/pemasok", pemasokRoutes);
app.use("/pelanggan", pelangganRoutes);
app.use("barang", barangRoutes);

// Run server

app.listen(3000, () => console.log("Server running on port 3000"));
