const express = require("express");

// import validator
const pelangganValidator = require("../middlewares/validators/pelangganValidator")

// import controllers
const pelangganController = require("../controllers/pelangganController");

// make routers
const router = express.Router();

// get all pelanggan
router.get("/", pelangganController.getAll);
router.get("/:id", pelangganValidator.getOne, pelangganController.getOne);
router.post("/", pelangganController.create);
router.put("/:id", pelangganValidator.update, pelangganController.update);
router.delete("/:id", pelangganController.delete);

module.exports = router;

