const { barang, pelanggan, pemasok } = require("../models"); // Import all models
require("../utils/associations");

class BarangController {
  // Get all barang
  async getAll(req, res) {
    try {
      let data = await barang.findAll({
        // find all data of barang table
        attributes: [
          "id",
          "nama",
          "harga",
          "image",
          "createdAt",
          "updatedAt",
          ["createdAt", "waktu"],
          ["updatedAt", "waktu"],
        ], // just these attributes that showed
        include: [
          // Include is join
          {
            model: pemasok,
            attributes: ["nama"], // just this attribute from Pemasok that showed
          },
        ],
      });

      // If data is nothing
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Get one barang
  async getOne(req, res) {
    try {
      let data = await barang.findOne({
        // find all data of barang table
        where: { id: req.params.id },
        attributes: [
          "id",
          "nama",
          "harga",
          "image",
          "createdAt",
          "updatedAt",
          ["createdAt", "waktu"],
          ["updatedAt", "waktu"],
        ], // just these attributes that showed
        include: [
          // Include is join
          { model: pemasok, attributes: ["nama"] }, // just this attribute from Pemasok that showed
        ],
      });

      // If data is nothing
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Create barang
  async create(req, res) {
    try {
      let createdData = await barang.create({
        nama: req.body.nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.body.image && req.body.image,
      });

      let data = await barang.findOne({
        where: {
          id: createdData.id,
        },
        attributes: [
          "id",
          "harga",
          "image",
          "createdAt",
          "updatedAt",
          ["createdAt", "waktu"],
          ["updatedAt", "waktu"],
        ],
        include: [
          // Include is join
          { model: pemasok, attributes: ["nama"] },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // Update data barang
  async update(req, res) {
    let update = {
      nama: req.body.nama,
      harga: req.body.harga,
      id_pemasok: req.body.id_pemasok,
      // image: req.body.image && req.body.image,
    };

    try {
      // Barang table update data
      let updatedData = await barang.update(update, {
        where: {
          id: req.params.id,
        },
      });

      // Find the updated transaksi
      let data = await barang.findOne({
        where: { id: req.params.id },
        attributes: [
          "nama",
          "harga",
          "id_pemasok",
          "createdAt",
          "updatedAt",
          ["createdAt", "waktu"],
          ["updatedAt", "waktu"],
        ],
        include: [
          // Include is join
          { model: pemasok, attributes: ["nama"] },
        ],
      });

      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // Delete Data
  async delete(req, res) {
    try {
      // Delete data
      let data = await barang.destroy({ where: { id: req.params.id } });

      // If data deleted is null
      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success delete transaksi",
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new BarangController();
