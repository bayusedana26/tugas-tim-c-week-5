const { pelanggan, pemasok, barang, transaksi } = require("../models");

class PemasokController {
  async getAll(req, res) {
    try {
      let data = await pemasok.findAll({
        attributes: ["id", "nama", "image", ["createdAt", "waktu terdaftar"]],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "Pemasok Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      let data = await pemasok.findOne({
        where: { id: `${req.params.id}` },
        attributes: ["id", "nama", "image", ["createdAt", "waktu terdaftar"]],
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
          },
        ],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "pemasok Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async create(req, res) {
    try {
      let creatingData = await pemasok.create({
        nama: req.body.nama,
        image: req.body.image && req.body.image,
      });
      console.log(creatingData);
      let data = await pemasok.findOne({
        where: { id: creatingData.id },
        attributes: ["nama", "image", ["createdAt", "waktu terdaftar"]],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "pemasok Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async delete(req, res) {
    try {
      let data = await pemasok.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      return res.status(200).json({
        message: "Success delete pemasok",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updateData(req, res) {
    let updates = {
      nama: req.body.nama,
      image: req.body.image && req.body.image,
    };
    console.log(updates);
    try {
      let updatedData = pemasok.update(updates, {
        where: { id: req.params.id },
      });
      let data = await pemasok.findOne({
        where: { id: req.params.id },
        attributes: [
          "id",
          "nama",
          "image",
          ["createdAt", "waktu terdaftar"],
          ["updatedAt", "terakhir di Update"],
        ],
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
          },
        ],
      });

      return res.status(201).json({
        message: "Successfull",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
module.exports = new PemasokController();
