const express = require("express");
const app = express();
const routesPemasok = require("./routes/routePemasok");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static("public"));

// Import table relationship
require("./utils/associations");

app.use("/pemasok", routesPemasok);

app.listen(3000, () => {
  console.log("server is great success! hi five!");
});
