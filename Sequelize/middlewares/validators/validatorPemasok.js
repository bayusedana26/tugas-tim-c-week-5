const { transaksi, barang, pelanggan, pemasok } = require("../../models");
const validator = require("validator");

class Validator {
  async create(req, res, next) {
    try {
      if (!validator.isAlphanumeric(req.body.nama)) {
        return res.status(400).json({
          messsage: "please put only alphabet or number without space",
        });
      }
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new Validator();
