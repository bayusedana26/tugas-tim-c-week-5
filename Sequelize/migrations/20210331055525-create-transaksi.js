"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("transaksi", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      id_barang: {
        type: Sequelize.INTEGER,
      },
      id_pelanggan: {
        type: Sequelize.INTEGER,
      },
      jumlah: {
        type: Sequelize.INTEGER,
      },
      total: {
        type: Sequelize.DECIMAL,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addConstraint("transaksi", {
      fields: ["id_barang"],
      type: "foreign key",
      name: "custom_fkey_id_barang",
      references: {
        //Required field
        table: "barang",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("transaksi", {
      fields: ["id_pelanggan"],
      type: "foreign key",
      name: "custom_fkey_id_pelanggan",
      references: {
        //Required field
        table: "pelanggan",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("transaksi");
  },
};
