const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const { imageUpload } = require("../middlewares/uploads/imageUpload");
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");

router.post("/", imageUpload, barangValidator.create, barangController.create);
router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.put("/:id", barangValidator.update, barangController.update);
router.delete("/:id", barangController.delete);

module.exports = router; // Export router
