const express = require("express");
const routes = express.Router();
const { imageUpload } = require("../middlewares/upload/imageUpload");
const validator = require("../middlewares/validators/pemasokValidator")

const controllerPemasok = require("../controllers/pemasokControllers");

routes.get("/", controllerPemasok.getAll);
routes.get("/:id", controllerPemasok.getOne);
routes.post("/", imageUpload, controllerPemasok.create);
routes.put("/:id", controllerPemasok.updateData);
routes.delete("/:id", controllerPemasok.delete);
module.exports = routes;
