const express = require("express");
const routes = express.Router();
const { imageUpload } = require("../middlewares/uploads/imageUpload");
const validator = require("../middlewares/validators")

const controllerPemasok = require("../controllers/controllersPemasok");

routes.get("/", controllerPemasok.getAll);
routes.get("/:id", controllerPemasok.getOne);
routes.post("/", imageUpload, controllerPemasok.create);
routes.put("/:id", controllerPemasok.updateData);
routes.delete("/:id", controllerPemasok.delete);
module.exports = routes;
