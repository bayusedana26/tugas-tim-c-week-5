"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("pelanggan", [
      {
        nama: "Bayu",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Dini",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Fitri",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("pelanggan", null, {});
  },
};
