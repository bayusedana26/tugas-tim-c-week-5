"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "resi",
      [
        {
          id_transaksi: 1,
          status: "lunas",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id_transaksi: 2,
          status: "lunas",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id_transaksi: 3,
          status: "lunas",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("resi", null, {});
  },
};
